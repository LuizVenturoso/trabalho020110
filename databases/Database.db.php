<?php

class Database {
    # Variável que guarda a conexão PDO.

    protected static $db;
    protected static $db_sem_bd;
    protected static $erro = "sucesso";

    # Private construct - garante que a classe só possa ser instanciada internamente.

    public function __construct() {

            # Informações sobre o banco de dados:
            $db_host = "localhost";
            $db_nome = "materias_construcao";
            $db_usuario = "root";
            $db_senha = '';
        

        $db_driver = "mysql";
        try {
            # Atribui o objeto PDO à variável $db.
            self::$db = new PDO("$db_driver:host=$db_host; dbname=$db_nome", $db_usuario, $db_senha);
            # Garante que o PDO lance exceções durante erros.
            self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            # Garante que os dados sejam armazenados com codificação UFT-8.
            self::$db->exec('SET NAMES utf8');
        } catch (PDOException $e) {
            # Envia um e-mail para o e-mail oficial do sistema, em caso de erro de conexão.
            //   mail($sistema_email, "PDOException em $sistema_titulo", $e->getMessage());
            # Então não carrega nada mais da página.
            self::$erro = "Erro não tratado: " . $e->getMessage();
        }
    }

    # Método estático - acessível sem instanciação.

    public static function conexao() {
        # Garante uma única instância. Se não existe uma conexão, criamos uma nova.
        if (!self::$db) {
            new Database();
        }
        # Retorna a conexão.
        return self::$db;
    }

    # Método estático - acessível sem instanciação.

    public static function v_erro() {
        Database::conexao();
        return self::$erro;
    }

}

//$pdo = Database::conexao();
//var_dump(Database::v_erro());

?>
