<?php
require_once './controlles/Config.class.php';
?>




<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>






<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>MHS - Construtora</title>
        <link rel="shortcut icon" sizes="114x114" href="img/favicon.fw.png" type="image/x-icon" />
        <meta name="description" content="Pushy is an off-canvas navigation menu for your website.">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

        <link rel="stylesheet" href="<?= HOST; ?>/css/normalize.css">
        <link rel="stylesheet" href="<?= HOST; ?>/css/demo.css">
        <!-- Pushy CSS -->
        <link rel="stylesheet" href="<?= HOST; ?>/css/pushy.css">
        <!-- Style CSS -->
        <link rel="stylesheet" href="<?= HOST; ?>/css/style.css">
        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    </head>
    <body>

        <header class="site-header push" style="height: 70px">
            <div class="menu-top">
                <!-- Menu Button -->
                <button class="menu-btn">&#9776; Menu</button>
            </div>
            <div class="conteudo-top">
                MHS <br>
                Construtora
            </div>

        </header>

        <!-- Pushy Menu -->
        <nav class="pushy pushy-left" data-focus="#first-link">
            <div class="pushy-content">
                <ul>
                    <li class="pushy-submenu">
                        
          
                    <li class="pushy-link"><a href="<?= HOST."/Colaboradores" ?>">Colaboradores</a></li>
                    <li class="pushy-link"><a href="<?= HOST."/Estoque" ?>">Estoque</a></li>
                     <li class="pushy-link"><a href="<?= HOST."/Crediario" ?>">Crediário</a></li>
                </ul>
            </div>
        </nav>

        <!-- Site Overlay -->
        <div class="site-overlay"></div>

        <!-- Your Content -->
        <div id="container">

            <?php
          //  var_dump($_GET);

            New Loader($_GET);
            ?>



        </div>



        <footer class="site-footer push">Desenvolvido por MHS - Sistemas inteligentes.</footer>

        <!-- Pushy JS -->
        <script src="<?= HOST; ?>/js/pushy.min.js"></script>

    </body>
</html>


