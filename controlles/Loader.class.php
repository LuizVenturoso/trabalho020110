<?php

class Loader {

    private $erro = 0;
    private $dados = array();

    public function __construct($url) {

        //Verifica se a classe existe
        if (isset($url['url'])) {
            $url = explode("/", $url['url']);
          //  var_dump($url);
            //Verifica se a classe existe
            $url[0] = ucfirst($url[0]);
            if (class_exists($url[0])) {
                $controller = New $url[0];
                //var_dump($url);
                //Verifica se o Metodu existe
                isset($url[1]) ? $url[1] = $url[1] : $url[1] = null;
                if (method_exists($controller, $url[1])) {
                    
                    isset($url[2]) ? $url[2] = $url[2] : $url[2] = null;
                    array_push($this->dados, $url[2]);
                    //Chama o controler responsavel
                    call_user_func_array(array($controller, $url[1]), $this->dados);
                } else {
                    call_user_func_array(array($controller, "index"), $this->dados);
                }
            } else {
                $this->erro = 1;
            }
        } else {
            $this->erro = 1;
        }
        //Se houve algum erro inclui a index da classe
        $this->erro == 1 ? (new Colaboradores())->index("index") : 0;
    }

}

?>